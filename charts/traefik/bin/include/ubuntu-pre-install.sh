#!/usr/bin/env bash -e

realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}
SCRIPT_PATH=$(realpath $0)
INCLUDE_DIRECTORY=$(dirname ${SCRIPT_PATH})
BIN_DIRECTORY=$(dirname ${INCLUDE_DIRECTORY})
CONFIG_DIRECTORY=$(dirname ${BIN_DIRECTORY})

local_domain=traefik.local

echo "Adding ${local_domain} certificate trusted certificates"
sudo cp ${CONFIG_DIRECTORY}/environments/local/traefik/bundle.pem /usr/local/share/ca-certificates/traefik.local.bundle.crt
sudo cp ${CONFIG_DIRECTORY}/environments/local/traefik/cert.pem /usr/local/share/ca-certificates/traefik.local.cert.pem
sudo update-ca-certificates --fresh
certutil -d sql:$HOME/.pki/nssdb -A -t "P,," -n /usr/local/share/ca-certificates/traefik.local.cert.pem -i /usr/local/share/ca-certificates/traefik.local.cert.pem