#!/usr/bin/env bash -e

realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}
SCRIPT_PATH=$(realpath $0)
INCLUDE_DIRECTORY=$(dirname ${SCRIPT_PATH})
BIN_DIRECTORY=$(dirname ${INCLUDE_DIRECTORY})
CONFIG_DIRECTORY=$(dirname ${BIN_DIRECTORY})

local_domain=traefik.local

echo "Adding ${local_domain} certificate to keychain"
sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain ${CONFIG_DIRECTORY}/environments/local/traefik/bundle.pem