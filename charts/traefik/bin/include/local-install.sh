#!/usr/bin/env bash -e

realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}
SCRIPT_PATH=$(realpath $0)
INCLUDE_DIRECTORY=$(dirname ${SCRIPT_PATH})
BIN_DIRECTORY=$(dirname ${INCLUDE_DIRECTORY})
CONFIG_DIRECTORY=$(dirname ${BIN_DIRECTORY})

local_domain=$1

chart_name=$(basename ${CONFIG_DIRECTORY})

if [[ -z "${local_domain}" ]];then
    echo "Missing argument 1 for local_domain"
    exit 1
fi

if [[ -z $(helm ls | grep ${chart_name}) ]]; then
    helm install "${CONFIG_DIRECTORY}" --name ${chart_name} --namespace kube-system
fi

echo "Waiting for service to become available"
while [[ "$(curl -s -o /dev/null -w '%{http_code}' https://${local_domain}/dashboard/)" != "200" ]]; do sleep 1; done

echo "Service available on https://${local_domain}"

open "https://${local_domain}"