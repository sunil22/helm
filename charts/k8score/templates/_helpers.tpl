{{/* vim: set filetype=mustache: */}}

{{/*
Expand the name of the chart.
*/}}
{{- define "k8score.name" -}}
{{- .Chart.Name | trunc 63 | trimSuffix "-" -}}
{{- end -}}


{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "k8score.fullname" -}}
{{- $name := .Chart.Name -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}

{{/*
Create the block for the ProxyProtocol's Trusted IPs.
*/}}
{{- define "k8score.trustedips" -}}
         trustedIPs = [
	   {{- range $idx, $ips := .Values.proxyProtocol.trustedIPs }}
	     {{- if $idx }}, {{ end }}
	     {{- $ips | quote }}
	   {{- end -}}
         ]
{{- end -}}
{{/*
Create the block for the forwardedHeaders's Trusted IPs.
*/}}
{{- define "k8score.forwardedHeadersTrustedIPs" -}}
         trustedIPs = [
	   {{- range $idx, $ips := .Values.forwardedHeaders.trustedIPs }}
	     {{- if $idx }}, {{ end }}
	     {{- $ips | quote }}
	   {{- end -}}
         ]
{{- end -}}

{{/*
Create the block for whiteListSourceRange.
*/}}
{{- define "k8score.whiteListSourceRange" -}}
       whiteListSourceRange = [
	   {{- range $idx, $ips := .Values.whiteListSourceRange }}
	     {{- if $idx }}, {{ end }}
	     {{- $ips | quote }}
	   {{- end -}}
         ]
{{- end -}}

{{/*
Create the block for acme.domains.
*/}}
{{- define "k8score.acme.domains" -}}
{{- range $idx, $value := .Values.acme.domains.domainsList }}
    {{- if $value.main }}
    [[acme.domains]]
       main = {{- range $mainIdx, $mainValue := $value }} {{ $mainValue | quote }}{{- end -}}
    {{- end -}}
{{- if $value.sans }}
       sans = [
  {{- range $sansIdx, $domains := $value.sans }}
			 {{- if $sansIdx }}, {{ end }}
	     {{- $domains | quote }}
  {{- end -}}
	     ]
	{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create custom cipherSuites block
*/}}
{{- define "k8score.ssl.cipherSuites" -}}
          cipherSuites = [
          {{- range $idx, $cipher := .Values.ssl.cipherSuites }}
            {{- if $idx }},{{ end }}
            {{ $cipher | quote }}
          {{- end }}
          ]
{{- end -}}

Create the block for RootCAs.
*/}}
{{- define "k8score.rootCAs" -}}
         rootCAs = [
	   {{- range $idx, $ca := .Values.rootCAs }}
	     {{- if $idx }}, {{ end }}
	     {{- $ca | quote }}
	   {{- end -}}
         ]
{{- end -}}

{{/*
Common labels
*/}}
{{- define "k8score.labels" -}}
app: {{ template "k8score.name" . }}
chart: "{{ .Chart.Name }}-{{ .Chart.Version }}"
release: {{ .Release.Name | quote }}
heritage: {{ .Release.Service | quote }}
{{- end -}}