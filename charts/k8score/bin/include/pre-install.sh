#!/usr/bin/env bash -e

realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}
SCRIPT_PATH=$(realpath $0)
INCLUDE_DIRECTORY=$(dirname ${SCRIPT_PATH})
BIN_DIRECTORY=$(dirname ${INCLUDE_DIRECTORY})
CONFIG_DIRECTORY=$(dirname ${BIN_DIRECTORY})

${CONFIG_DIRECTORY}/charts/istiocustom/bin/include/pre-install.sh
${CONFIG_DIRECTORY}/charts/traefik/bin/include/pre-install.sh

${CONFIG_DIRECTORY}/charts/istiocustom/bin/install.sh