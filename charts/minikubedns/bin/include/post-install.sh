#!/usr/bin/env bash -e
realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}
SCRIPT_PATH=$(realpath $0)
INCLUDE_DIRECTORY=$(dirname ${SCRIPT_PATH})
BIN_DIRECTORY=$(dirname ${INCLUDE_DIRECTORY})
CONFIG_DIRECTORY=$(dirname ${BIN_DIRECTORY})

kubectl apply -f ${CONFIG_DIRECTORY}/overrides
kubectl patch deployment nginx-ingress-controller -n kube-system --patch '{"spec":{"template":{"spec":{"hostNetwork":true}}}}'