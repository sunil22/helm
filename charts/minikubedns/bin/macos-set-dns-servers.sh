#!/usr/bin/env bash -e

trim () {
   echo $1 | sed 's/ *$//g' | tr -d '\n' | tr -d '\t' | tr -d ' '
}

function join { local IFS="$1"; shift; echo "$*"; }

minikube_ip=$(minikube ip)
trimmed_minikube_ip=$(trim "${minikube_ip}")
new_dns_servers=("${minikube_ip} ")

dns_servers_raw=$(networksetup -getdnsservers Wi-Fi)
readarray -t dns_servers <<<"$dns_servers_raw"
for dns_server in "${dns_servers[@]}"
do
    trimmed_dns_server=$(trim "${dns_server}")

    if [[ "${trimmed_dns_server}" != "${trimmed_minikube_ip}" ]]; then
        new_dns_servers+="${trimmed_dns_server} "
    fi
done
dns_servers_input=$(join " " ${new_dns_servers[@]})
networksetup -setdnsservers Wi-Fi ${dns_servers_input}