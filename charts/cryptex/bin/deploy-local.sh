#!/bin/bash -e
realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}
script_path=$(realpath "$0")
bin_directory=$(dirname "${script_path}")

"${bin_directory}/include/deploy.sh" local