#!/bin/bash -e
realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}
script_path=$(realpath "$0")
include_directory=$(dirname "${script_path}")
bin_directory=$(dirname "${include_directory}")
config_directory=$(dirname "${bin_directory}")

# Colors
green='\033[0;32m'
yellow='\033[0;93m'
no_color='\033[0m'
cyan='\033[0;36m'
red='\033[0;31m'

# Inputs
environment=$1
if [[ -z "${environment}" ]]; then
    echo -e "${red}Missing arg 1 for environment name${no_color}"
    exit 1
fi

cd "${config_directory}"
helm repo add elastic https://helm.elastic.co
helm dep up
chart_name=cryptex
namespace=cryptex-system

if [[ ! $(helm ls | grep ${chart_name}) ]]; then
    helm install "${config_directory}" \
    --name ${chart_name} \
    --namespace ${namespace} \
    --values "${config_directory}/${environment}.values.yaml" \
    --values "${config_directory}/values.yaml" \
    --debug
else
    helm upgrade ${chart_name} \
    "${config_directory}" \
    --namespace ${namespace} \
    --values "${config_directory}/${environment}.values.yaml" \
    --values "${config_directory}/values.yaml" \
    --debug
fi