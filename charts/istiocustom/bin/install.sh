#!/usr/bin/env bash -e

realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}
SCRIPT_PATH=$(realpath $0)
BIN_DIRECTORY=$(dirname ${SCRIPT_PATH})
CONFIG_DIRECTORY=$(dirname ${BIN_DIRECTORY})

echo "Installing istio in istio-system namespace"
helm repo add istio https://storage.googleapis.com/istio-release/releases/1.2.2/charts/
helm install ${CONFIG_DIRECTORY} --name istio --namespace istio-system